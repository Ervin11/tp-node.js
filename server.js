const express = require('express')
const axios = require('axios');
const app = express()
const port = 3000

var bodyParser = require('body-parser')
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Le serveur écoute sur le port: ${port}`)
})

app.get('/users', async (req, res) => {
    let users = await axios.get(`https://jsonplaceholder.typicode.com/users`)
    res.send(users.data)
})

app.get('/users/:id', async (req, res) => {
    
    let user = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}`)
    res.send(user.data)
})

app.post('/users', async (req, res) => {
    
    let createUser = await axios.post(`https://jsonplaceholder.typicode.com/users`, req.body)
    res.send(createUser.data)
})

app.patch('/users/:id', async (req, res) => {

    let updateUser = await axios.patch(`https://jsonplaceholder.typicode.com/users/${req.params.id}`, req.body)
    res.send(updateUser.data)
})

app.put('/users/:id', async (req, res) => {

    let updateUser = await axios.put(`https://jsonplaceholder.typicode.com/users/${req.params.id}`, req.body)
    res.send(updateUser.data)
})

app.delete('/users/:id', async (req, res) => {

    let deleteUser = await axios.delete(`https://jsonplaceholder.typicode.com/users/${req.params.id}`)
    res.send(deleteUser.data)
})

app.get('/users/:id/posts', async (req, res) => {
    let userPosts = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}/posts`)
    res.send(userPosts.data)
})

app.get('/users/:id/albums', async (req, res) => {
    
    let user = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}`)           
    let albums = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}/albums`)
    let photos = await axios.get(`https://jsonplaceholder.typicode.com/photos`)           
    let albumPhotos = []    
    
    for (album of albums.data) {        
        albumPhotos = []        
        for (photo of photos.data) {
            if (album.id == photo.albumId) { 
                albumPhotos.push(photo)                               
            }            
        }
        album['photos'] = albumPhotos
    }

    user.data['albums'] = albums.data

    res.send(user.data)    
})
